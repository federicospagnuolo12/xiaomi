<?php
	require_once("back-end/conexion.php");
    session_start();
	if(isset($_SESSION['nueva'])){
	    $_SESSION['nueva'];
    }
	if(isset($_COOKIE['email'])){
        $_COOKIE['email'];	    
    }
	if (isset($_GET['id_productos'])){
		$id=$_GET['id_productos'];
		$sql="SELECT * FROM productos  WHERE id_productos='".$id."'"; 
		$consulta=mysqli_query($conexion,$sql);
		while ($registro=mysqli_fetch_assoc($consulta)){
            $nombre=$registro['nombre'];
            $precio=$registro['precio'];
			$foto=$registro['IMG'];
			$envio=350;
        	$pagoTotal=($envio+$precio);

		}
    }
    	else{
         echo '<script>
	        location.replace("http://xiaomiztore.000webhostapp.com/index.php?noInisiado#modal2");
	    </script>';
	}
	    	// CUENTA DE TOTALES //


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">		
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/indexx.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link href="imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">	
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/favoritos.css">
	<link rel="stylesheet" type="text/css" href="font/iconos/style.css">
    <link rel="stylesheet" href="css/iniciarSecion.css" />
    <link rel="stylesheet" href="css/flag-icon.css" />
    <link rel="stylesheet" href="css/flag-icon.main.css" />
    
    <link rel="stylesheet" type="text/css" href="css/confirmarCompra.css">
    <title>Confirma tu pedido</title>
</head>
<body>
<?php
	if (isset($_SESSION['nueva'])) {
		echo '<nav class="menuPrincipal">';
			include("includes/menu_sesion.php"); 
		echo '</nav>';	}
	else{
	    echo '<script>
	        location.replace("http://xiaomiztore.000webhostapp.com/index.php?noInisiado#modal2");
	    </script>';
	}
    ?>
    <div class="contendorCompra">
        <div class="infoCompra">
            <form action="">
               <div class="cont"> <div class="cajasCompras" style=" margin-top: 5%">
                    <h1>Información de envío</h1>
                    <label for="">Contacto</label>
                    <div class="contacto">
                        <div class="inputContactoIzq">
                            <input type="text" name="name" placeholder="Nombre">
                        </div>
                        <div class="inputContactoDer">
                            <input type="text" name="area" value="+54">
                            <input type="text" name="phone" placeholder="Telefono / Celular">
                        </div>
                                              
                    </div>
                    <label for="">Dirección</label>
                    <div class="direccion">
                        <div class="inputDireccionTop"> 
                            <input type="text" name="calle" placeholder="Calle">
                            <input type="text" name="altura" placeholder="Numero , piso , etc..">
                        </div>
                        <div class="inputDireccionBottom">
                           <div class="pais">
                                <span  class = " flag-icon flag-icon-ar " ></span>
                                <span style="margin-left: 5px">Argentina</span>
                           </div>
                           <select name="provincias">
                                <option value="">Provincias</option>
                                <option value="">Ciudad Autonoma de Buenos Aires (CABA)</option>
                                <option value="">Area Metropolitana de Buenos Aires (AMBA)</option>
                                <option value="">Catamarca</option>
                                <option value="">Chaco</option>
                                <option value="">Chubut</option>
                                <option value="">Córdoba</option>
                                <option value="">Corrientes</option>
                                <option value="">Entre Ríos</option>
                                <option value="">Formosa</option>
                                <option value="">Jujuy</option>
                                <option value="">La Pampa</option>
                                <option value="">La Rioja</option>
                                <option value="">Mendoza</option>
                                <option value="">Misiones</option>
                                <option value="">Neuquén</option>
                                <option value="">Río Negro</option>
                                <option value="">Salta</option>
                                <option value="">San Juan</option>
                                <option value="">San Luis</option>
                                <option value="">Santa Cruz</option>
                                <option value="">Santa Fe</option>
                                <option value="">Santiago del Estero</option>
                                <option value="">Tierra del Fuego</option>
                                <option value="">Tucumán</option>
                            </select>
                            <input type="text" nane="city" placeholder="Ciudad"></div>
                        </div>
                        <div class="cp">
                            <input type="text" name="CP" placeholder="Codigo Postal">
                        </div>
                    </div>
                <div class="cajasCompras">
                    <h1>Método de pago</h1>
                    <div class="tarjeta">
                        <div class="tarjetaFrontal">
                            <div class="numTarjeta">
                                <label for="">N.º de tarjeta</label>
                                <input type="text" name="tarjeta" placeholder="0000 0000 0000 0000">
                            </div>
                            <div class="tarje">
                                <div class="datosTarje"> 
                                    <label for=""> TITULAR DE LA TARJETA</label>
                                    <input type="text" name="nombreTarjeta" placeholder="NOMBRE">
                                </div>
                                <div class="datosTarje"> 
                                    <label for="">CADUCIDAD</label>
                                    <input type="text" name="vencimiento" placeholder="MM/YY">
                                </div>
                            </div>
                        </div>
                        <div class="tarjetaPosterior">
                            <div class="barraTarjeta"></div>
                            <div class="cvv">
                                <label for="">CVV</label>
                                <input type="text" name="cvv" placeholder="000">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cajasCompras">
                    <h1>Detalles del pedido</h1>
                    <div class="detallesPedido">
                        <div class="imagenPedido">
                            <?php 
                                echo '<img id="foto" src="imagenes/Miniaturas/'.$foto.'">';
                                echo '<span class="tituloCelular" id="tituloMovil" style="display:none"><h1>'.$nombre.'</h1></span>';
                            ?>
                        </div>    
                        
                        <div class="textPedido">	
                            <span class="tituloCelular"><?php echo '<h1>'.$nombre.'</h1>'; ?></span>
                            <h2 class="precio"><?php echo '<p class="p">ARS</p> $'.$precio;?></h2>
                            <span class="envio"> <p class="p">Envío:</p> ARG $0.00 vía Correo Argentino.</span>
                            <span class="envio"> <p class="p">Tiempo de entrega:</p> 30-60 días</span>		
                        </div>
                    </div> 
                </div></div>
                <div class="precioEstimado">
                    <div class="cajaEstimada">
                        <div class="cerrarDetalles"><i class="fas fa-caret-down" id="cerrarDetallesCarri"></i></div>
                        <h1>Resumen del pedido</h1>
                        <div class="datos">
                            <div class="cajas">
                                <p>Subtotal</p>
                                <p>Envio</p>
                                
                            </div>
                            <div class="cajas">
                                <p class="valores">ARS $<?php echo number_format($precio)?></p>
                                <p class="valores">ARS $<?php echo $envio?></p>
                                
                            </div>
                        </div>
                        <hr class="hr-datos">
                        <div class="total">
                                <p>Total</p>
                                <h3>ARS $<?php echo number_format($pagoTotal);?></h3>
                        </div><div class="detallesCarrito"><i class="fas fa-sort-up" id="detallesCarri"></i></div>

                        <div class="confirmar">
                            <input type="submit" value="CONFIRMAR COMPRA">
                        </div>
                    </div>
	        	</div>
            </form>
        </div> 
    </div>
    <footer class="Footer">
		<?php 
			include("includes/footer.html");
		?>
    </footer>
    <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
</body>
</html>