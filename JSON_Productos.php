<?php
    require_once("back-end/conexion.php");
    header('Content-Type: application/json');
    $devolver_array = array();
    $sql = "SELECT * FROM productos";
    $consulta = mysqli_query($conexion,$sql);
    
    while($registro = mysqli_fetch_array($consulta)){
        $id = $registro['id_productos'];
        $producto = $registro['nombre'];
        $precio = $registro['precio'];
        
        $devolver_array[] = array("id" => $id,
                        "producto" => $producto,
                        "precio" => $precio);
    }
    
    echo json_encode($devolver_array);
    exit;
?>







