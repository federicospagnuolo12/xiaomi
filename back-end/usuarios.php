<?php 
	require_once('conexion.php');
	$sql="SELECT * FROM Usuarios";
	$consulta=mysqli_query($conexion,$sql);
    if (isset($_GET["id_borrar"])) {
    	include ("borrar_usuario.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <link href="../imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">
	<meta charset="UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="jquery-3.4.1.min.js"></script>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="eestilos.css">
	<link rel="stylesheet" type="text/css" href="../css/menu.css">
	<link rel="stylesheet" type="text/css" href="../font/iconos/style.css">
	
	<title>Usuarios</title>
</head>
<body>
	<header>
			<div class="menuPrincipal">
				<div class="menuPrincipal_Logo">
					<a href="Agregar_productos.php"><img src="../imagenes/mihome.png"></a>
					<h1>Xiaomi</h1>
					<div class="menuPrincipal_Logo_sub">
						<div class="sub">
						
							<ul>
								<li>
									<a href="Agregar_productos.php">Productos</a>
									<ul>
										<li><a href="Agregar_productos.php?familia=mi">MI</a></li>
										<li><a href="Agregar_productos.php?familia=redmi">Redmi</a></li>
										<li><a href="Agregar_productos.php?familia=poco">Poco</a></li>
										<li><a href="Agregar_productos.php">Todos</a></li>
									</ul>
									
								</li>
							</ul>
							<a href="usuarios.php">Usuarios</a>
						</div>

					</div>
				</div>
				<div class="menuPrincipal_Busqueda">
					<form action="" method="get">	
						<input type="search" placeholder="Seach...">
						<div class="icono"><a href=""><span class="lnr lnr-magnifier"></span></a></div>
					</form>
				</div>
			</div>
	</header>
	<div class="contUser" id="fullHeight">
		<h1>USUARIOS</h1>
		<hr class="hr-back" id='hrUser'>
		<div class="user">
			<?php  	
				if (mysqli_num_rows($consulta)>0) {
				while ($registro=mysqli_fetch_assoc($consulta)) {
			?>
			<div class="contbot">			
					<ul>
					<div class="bott">
						<?php
							echo '<span>'.$registro['nombre_usuario'].'</span>';
						?>
						<p class="items">Email: <?php echo $registro['email_usuario']; ?></p>
                        <p class="items">Ultima conexión: <?php echo $registro['ultima_conexion']?></p>
                        <p class="items">Estado: 
                        		<?php
                        			if($registro['verificado'] == 'verificado'){?>
                        			<span style="color:green;font-weight:bold;"><?php echo $registro['verificado']; ?></span>
                        			<?php
                        			 
                        			}else{?>
                        					<span style="color:red;font-weight:bold;"><?php echo $registro['verificado']; ?></span>
                        			<?php
                        				} 
                        		    ?>
                        	</p>	

					</div>
					<div class="bot">
							<a href="usuarios.php?id_editar=<?php echo($registro['id_usuario'])?>"><span class="lnr lnr-pencil"></span></a>
							<a href="usuarios.php?id_borrar=<?php echo($registro['id_usuario'])?>"><span class="lnr lnr-trash"></span></a>
					</div>
				</ul>
			</div>
			<?php
				}
			}else{
				echo "no hay usuarios";
			}
			?>
		</div>
	</div>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();

        $("#fullHeight").height(height - 250);
      });
    </script>
</body>
</html>
	
