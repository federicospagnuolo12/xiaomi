<?php
    require_once("back-end/conexion.php");
    session_start();
	if(isset($_SESSION['nueva'])){
	    $_SESSION['nueva'];
    }
	if(isset($_COOKIE['email'])){
        $_COOKIE['email'];	    
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">		
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/indexx.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link href="imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">	
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="font/iconos/style.css">
	<link rel="stylesheet" href="css/iniciarSecion.css" />
	<style>
    	img[src="https://cdn.000webhost.com/000webhost/logo/footer-powered-by-000webhost-white2.png"]{ display:none}
	</style>

	

	<title>Xiaomi Argentina</title>
</head>
<body>

	<?php
	if (isset($_SESSION['nueva'])) {
		echo '<nav class="menuPrincipal">';
			include("includes/menu_sesion.php"); 
		echo '</nav>';	}
	else{
		echo '<nav class="menuPrincipal">';
				include("includes/menu.php"); 
		echo '</nav>';
	}
	?>
	<!------------ INFORMACION ------------>

	<?php
	include("includes/contenido.html"); 
	 ?>

	<!------------ FOOTER ------------> 
	<footer class="Footer">
		<?php 
			include("includes/footer.html");
		?>
	</footer>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
</body>
</html>