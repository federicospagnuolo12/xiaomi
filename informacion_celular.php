<?php
	require_once("back-end/conexion.php");
    session_start();
	if(isset($_SESSION['nueva'])){
	    $_SESSION['nueva'];
    }
	if(isset($_COOKIE['email'])){
        $_COOKIE['email'];	    
    }
	if (isset($_GET['id_productos'])){
		$id=$_GET['id_productos'];
		$sql="SELECT * FROM productos  WHERE id_productos='".$id."'"; 
		$consulta=mysqli_query($conexion,$sql);
		while ($registro=mysqli_fetch_assoc($consulta)){
			$nombre=$registro['nombre'];
			$familia=$registro['familia'];
			$procesador=$registro['Procesador'];
			$pantalla=$registro['Pantalla'];
			$trasera=$registro['CamaraTrasera'];
			$frontal=$registro['CamaraFrontal'];
			$bateria=$registro['Bateria'];
			$android=$registro['Android'];
			$almacenamiento=$registro['Almacenamiento'];
			$foto=$registro['colo'];

		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<link href="imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">
	<title>Informacion Celular - <?php echo $nombre; ?></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="jquery-3.4.1.min.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">	
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:600&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="font/iconos/style.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/celular_info.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link rel="stylesheet" href="css/iniciarSecion.css" />
	<meta charset="utf-8">

	<!--<meta http-equiv="refresh" content="1">-->
</head>
<body>

<header>
	<?php
	if (isset($_SESSION['nueva'])) {
		echo '<nav class="menuPrincipal">';
			include("includes/menu_sesion.php"); 
		echo '</nav>';	}
	else{
		echo '<nav class="menuPrincipal">';
				include("includes/menu.php"); 
		echo '</nav>';
	}
	?>
</header>

	<div id="padre">
		<div id="celu_img">
			<div id="fotoPrincipal">
				<?php echo '<img id="foto" src="imagenes/celulares/'.$foto.'">';?>
			</div>
			<span class="tituloCelular" id="tituloMovil"><?php echo '<h1>'.$nombre.'</h1>'; ?></span>
		</div>
		<div id="celu_info">
			<span class="tituloCelular"><?php echo '<h1>'.$nombre.'</h1>'; ?></span>
			<!--<form action="informacion_celular.php">
				<select>
					<option>Rojo</option>
					<option>Negro</option>
					<option>Dorado</option>
				</select>
			</form>-->
			<div id="caracteristicas">
				<ul class="caracteristicasCelular">
					<li><h2>Procesador: </h2> <?php echo '<div class="iconos"><i class="fas fa-brain"></i><span>'.$procesador.'</span></div>'; ?> </li>
					<li><h2>Pantalla: </h2> <?php echo '<div class="iconos"><i class="fas fa-mobile"></i><span>'.$pantalla.'</span></div>'; ?> </li>
					<li><h2>Camara trasera: </h2> <?php echo '<div class="iconos"><i class="fas fa-camera"></i><span>'.$trasera.'</span></div>'; ?> </li>
					<li><h2>Camara frontal: </h2> <?php echo '<div class="iconos"><i class="fas fa-portrait"></i><span>'.$frontal.'</span></div>'; ?> </li>
					<li><h2>Bateria: </h2> <?php echo '<div class="iconos"><i class="fas fa-battery-full"></i><span>'.$bateria.'</span></div>'; ?> </li>
					<li><h2>Sistema operativo: </h2><?php echo '<div class="iconos"><i class="fab fa-android"></i><span>'.$android.'</span></div>'; ?> </li>
					<li><h2>Almacenamiento: </h2> <?php echo '<div class="iconos"><i class="fas fa-memory"></i><span>'.$almacenamiento.'</span></div>'; ?> </li>
				</ul>
			</div>
			<div class="botones">
			    <?php
					echo '<a class="comprarInfo" href="confirmarCompra.php?id_productos='.$id.'"><div class="boton_compra"><p>Comprar ahora</p></div></a>';
					echo '<a class="comprarInfo" href="?carrito='.$id.'"><div class="boton_carrito"><p>Agregar al carrito</p></div></a>';
				?>
			</div>

		</div>
	</div>
	<footer class="Footer">
		<?php 
			include("includes/footer.html");
		?>
	</footer>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();

        $("#celu_img").height(height - 100);
      });
	</script>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();
        $("#foto").height(height - 100);
      });
	</script>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
</body>
</html>