<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <table id="tabla_prod" border="1" >
            <thead>
                <tr>
                    <th width="5%">Id</th>
                    <th width="20%">Nombre</th>
                    <th width="20%">Precio</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $.ajax({
                url: 'JSON_Productos.php',
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = response.length;
                    for(var i=0; i<len; i++){
                        var id = response[i].id;
                        var nombre = response[i].producto;
                        var precio = response[i].precio;
        
                        var datos = "<tr>" +
                        "<td align='center'>" + id + "</td>" +
                        "<td align='center'>" + nombre + "</td>" +
                        "<td align='center'>" + precio + "</td>" +
                        "</tr>";
                        
                        $("#tabla_prod tbody").append(datos);
                    }
                }
            });
        });
    </script>
</body>
</html>