<?php
	function redimensionarImagen($imagen, $ancho_final){
		list($ancho_orig, $alto_orig, $numero_tipo) = getimagesize($imagen);
		switch ($numero_tipo) {
			case 2:
				$img_original=imagecreatefromjpeg($imagen);
				break;
			
			case 3:
				$img_original=imagecreatefrompng($imagen);
				break;
			default:
				echo "tipo de archivo incorrecto";
				break;
		}
		$aspecto=$ancho_orig/$alto_orig;
		$alto_final=$ancho_final/$aspecto;
		$nueva_img=imagecreatetruecolor($ancho_final, $alto_final);
		imagecopyresampled($nueva_img, $img_original, 0, 0, 0, 0, $ancho_final, $alto_final, $ancho_orig, $alto_orig);
		imagedestroy($img_original);
		$calidad=70;
		$nombre_img=time()."-".$imagen;
		imagejpeg($nueva_img,"../imagenes/$nombre_img",$calidad);
		return $nombre_img;
	}
?>