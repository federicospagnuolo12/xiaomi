$(function () {
	$('.btn_insert').css({'display':'none'});	
	$('.btn_insert').click(function(){
		$('.contactos').css({'display':'none'});
		$('.listado').css({'display':'none'});
		$('.insert').css({'display':'block'});
		$('.editar').css({'display':'block'});
		$('.btn_insert').css({'display':'none'});
		$('.btn_list').css({'display':'block'});
	})
	$('.btn_list').click(function(){
		$('.contactos').css({'display':'block'});
		$('.listado').css({'display':'block'});
		$('.insert').css({'display':'none'});
		$('.editar').css({'display':'none'});
		$('.btn_insert').css({'display':'block'});
		$('.btn_list').css({'display':'none'});
	})
	var c=1;
	$(".btn-menu").click(function(){
		if (c==1) {
			$("nav").animate({'left':'0%','transition':'0.5s'});
			c=0;
		}
		else{
			$("nav").animate({'left':'-100%','transition':'0.5s'});
			c=1;
		}
	})

      $("#show-modal").click(function() {
    $("body").css("overflow", "hidden");
  });
  $("#show-modal2").click(function() {
    $("body").css("overflow", "hidden");
  });
  $("#show-modal3").click(function() {
    $("body").css("overflow", "hidden");
  });
  $("#show-modal4").click(function() {
    $("body").css("overflow", "hidden");
  });
    $("#detallesCarri").click(function() {
     $("body").css("overflow", "hidden");
    $(".cajaEstimada, .cajaEstimada .hr-datos").css(
      "display",
      "block",
      "padding",
      "5%"
    );
    $(".hr-datos ").css("margin", "0 auto 5% auto");
    $(".datos").css("display", "flex");
    $("#detallesCarri").css("display", "none");
    $(".cerrarDetalles").css("display", "flex");

    $(".total h3").css("margin-bottom", "5%  ");
    $(".cajaEstimada h1 , .cajaEstimada p").css("display", "block");
  });

  $("#cerrarDetallesCarri").click(function() {
    $("body").css("overflow", "auto");
    $("#detallesCarri").css("display", "block");
    $(".cajaEstimada").css("display", "flex");
    $(".cerrarDetalles").css("display", "none");

    $(
      ".cajaEstimada h1 , .cajaEstimada p , .cajaEstimada .datos , .cajaEstimada .hr-datos"
    ).css("display", "none");
  });
    var a = 0;
  $(".menuMovil").click(function() {
    if (a == 0) {
      $("#preparacionMovil").css("display", "block");
      document.getElementById("prueba").className = "fas fa-times";
      a = 1;
    } else {
      $("#preparacionMovil").css("display", "none");
      document.getElementById("prueba").className = "fas fa-bars";
      a = 0;
    }
  });
})