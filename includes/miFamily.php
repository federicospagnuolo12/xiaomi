<!DOCTYPE html>
<html>
<head>
	<title>Mi Global Home</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../css/miStyless.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">		
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/footer.css">
		<link rel="stylesheet" type="text/css" href="../font/iconos/style.css">
</head>
<body>
	<div class="contenedorMI">

		<!------------- TITULO DEL MODELO ------------->
		<div class="principalMI">
			<img src="../imagenes/mi/mi9se2.png" width="100%" height="auto">
			<div class="textMI">
				<div id="textM">
					<h1>MI 9 <span>SE</span></h1>
					<p>Tamano de bolsillo. Grandiosas Fotografias. 48MP. Con triple cámara.</p>
				</div>
				<div class="boton">
					<form action="lojin.php" method="post" enctype="multipart/form-data">

						<input type="submit" name="enviar" value="COMPRAR">

					</form>
					
				</div>
				<div class="infoMI">
					<P>Mi 9 SE fue creado para aquellos que valoran un teléfono de bolsillo. 
					Incorpora una pantalla AMOLED de Samsung, 
					sensor de huellas dactilares mejorado en la pantalla y cámara Sony de 48MP. 
					También cuenta con NFC, control remoto por infrarrojos y el estreno mundial de Qualcomm® Snapdragon ™ 712
					</P>	
				</div>
			</div>
		</div>

		<!-------------- 5G -------------->
		<div class="net5g">
			<img src="../imagenes/mi/net5G.jpg" width="100%" height="auto">
			<div class="text5G">
				<h1>Pionero de la red 5G. Velocidad de descarga +2GB/s</h1>
				<p>Como la principal insignia premiun de Xiaomi, Mi MIX 3 5G es uno de los primeros telefonos inteligentes 5G del mundo hechos para los primeros usuarios.</p>
			</div>
		</div>	

		<!-------------- PANTALLA CELULAR -------------->
		<div class="sreenMI">
			<img src="../imagenes/mi/screenMI9.png" width="100%" height="auto">
			<div class="textScreen">
				<h1 class="titleLed"> Pantalla completa Samsung AMOLED Dot Drop de 5.97"</h1>
				<p>Pantalla Samsung AMOLED de 5.97 "con una relación de pantalla / cuerpo de 90.47%.La amplia gama de colores NTSC 103.8% (típico) hace que los videos y las fotos sean más vibrantes. 
				La pantalla mantiene su claridad en luz brillante con el modo de luz solar 2.0. 
				que ajusta de manera inteligente el brillo de acuerdo con los rayos del sol, brindándole una experiencia rica y colorida en interiores y exteriores.</p>
				<div class="text2">
					<h1>90.47% relación <p>pantalla-cuerpo</p></h1>
					<h1>NTSC 103.8% <p>amplia gama de colores</p></h1>
					<h1>600nit (HBM) <p>Brillo</p></h1>
					<h1>> 60000: 1 <p>Alto contrasteo</p></h1>
				</div>
			</div>
		</div>

		<!-------------- TAMAÑO CELULAR -------------->
		<div class="tamañoMI">
			<h1>Más ligero, más pequeño y más sostenible.</h1>
			<p>Más liviano, con solo 155g, y el tamaño del teléfono tradicional de 5.1 ".Mi 9 SE presenta un cómodo agarre y uso con una sola mano en la era de la pantalla completa.</p>
			<img src="../imagenes/mi/tamañoMI9.jpg" width="85%" height="auto">
		</div>
		
		<!-------------- CAMARAS CELULAR -------------->
		<div class="camaraPrincipal">
			<img src="../imagenes/mi/camara.jpg" width="80%" height="auto">
			<div class="textPrincipal">
				<h1>Sony 48MP cámara principal </h1>
				<p>¿Qué significa tener una cámara con 4 veces más de píxeles? Cuando recorta una pequeña sección de una foto, el recorte mantiene una calidad detallada. Con el sensor de imagen de 1/2" y los 4 píxeles de 1,6 µm de tamaño en 1, las fotos son con coleres vivos.</p>
			</div>
		</div>

		<div class="camaraSecundaria">
			<div class="textSecundaria">
				<h1>Recortar para una imagen completamente nueva</h1>
				<p>Más píxeles significa una imagen más clara. La cámara insignia de Sony 48MP brinda una experiencia de alta resolución que te permite mantener la claridad incluso al recortar secciones pequeñas, lo que te permite crear una imagen completamente nueva.</p>
			</div>
			<img src="../imagenes/mi/macro.jpg" width="50%" height="50%">
		</div>

		<div class="camaraAngular">
			<img src="../imagenes/mi/angular.jpg" width="75%" height="auto">
			<div class="textAngular">
				<h1>Más que solo otra lente. Un ángulo más amplio y un disparo más cercano.</h1>
				<div class="ang"><p>El gran angular de 117 ° le permite capturar todo el paisaje en un solo disparo</p></div>
			</div>
		</div>

		<div class="camaraFrontal">
			<img src="../imagenes/mi/frontal.png" width="25%" height="auto">
			<div class="textFrontal">
				<h1>Cámara autofoto 20MP con embellecer</h1>
				<p>Autorretratos super claros con 20MP. 
				La cámara selfie no solo es compatible con Beautify y el maquillaje en 3D, sino que también puede agregar un brillo a sus ojos.</p>
			</div>

		</div>

		<!-------------- BATERIA CELULAR -------------->
		<div class="bateria">
			<div class="textBateria">
				<h1>Cargue más rápido con la primera carga rápida inalámbrica de 20 W del mundo. Carga inalámbrica como nunca antes había visto</h1>
				<p>Los días de carga inalámbrica lenta están en el pasado. Las nuevas innovaciones ahora han permitido alcanzar la carga completa en 90 minutos con la carga inalámbrica, que es incluso más rápida que la carga tradicional de 18W con cable.</p>
			</div>
			<img src="../imagenes/mi/bateria.png" width="55%" height="auto">

			<div class="bar">
				<div class="barText">
					<p>El cargador inalámbrico de 20W es 37% más rápido que un cargador rápido inalámbrico estándar de 10W</p>
					<img src="../imagenes/mi/bar.png" width="90%">
				</div>
				<div class="barText">
					<p>El cargador cableado de 27W es un 34% más rápido que un cargador rápido cableado estándar de 18W</p>
					<img src="../imagenes/mi/bar.png" width="90%">
				</div>
			</div>
		</div>

		<!-------------- PROCESADOR CELULAR -------------->
		<div class="procesador">
			<img src="../imagenes/mi/cpu.jpg" width="100%" height="auto">
			<div class="textProcesador">
				<h1>Qualcomm® Snapdragon ™ 845 Procesador insignia</h1>
				<p>El Snapdragon ™ 845 es otro paso adelante en el rendimiento de Qualcomm®. Ya sea que esté cambiando entre varias aplicaciones o luchando contra el enemigo en un juego intenso, este procesador es una herramienta indispensable.</p>
			</div>
		</div>

		<!-------------- SENSORES CELULAR -------------->
		<div class="nfc">
				<div class="textNfc">
					<h1>NFC multifuncional</h1>
					<p>No solo su teléfono: su billetera también es compatible con Android Pay</p>
				</div>
				<img src="../imagenes/mi/nfc.jpg" width="50%" height="50%" style="margin: 0px">
		</div>

		<div class="huella">
				<img src="../imagenes/mi/huella.jpg" width="50%" height="50%" style="margin: 0px">
				<div class="textHuella">
					<h1>El primer sensor de huellas dactilares en pantalla sensible a la presión del mundo</h1>
					<p>Un sensor de presión altamente sensible que mejora la tasa de reconocimiento de huellas dactilares. Al desbloquear, también vibra el dedo para completar la experiencia de desbloqueo.</p>
				</div>			
		</div>
		
		<div class="facial">
				<div class="textFacial">
					<h1>Desbloqueo facial con luz estructurada 3D</h1>
					<p>La vanguardia de la tecnología de reconocimiento facial en el espacio de una pulgada cuadrada. Una cámara de infrarrojos, un iluminador de inundación, un proyector de puntos y más se combinan para formar el escáner 3D de luz estructurada. Es una forma más segura de desbloquear de un vistazo</p>
				</div>
				<img src="../imagenes/mi/facial.jpg" width="50%" height="50%" style="margin: 0px">
		</div>

		<div class="gps">
				<img src="../imagenes/mi/gps.jpg" width="100%" height="100%">
				<div class="textGps">
					<h1>GPS de doble frecuencia.</h1>
					<h1>Navegación excepcionalmente precisa</h1>
					<p>Teléfono inteligente equipado con GPS de doble frecuencia. 
				Al utilizar las señales L1 y L5 en coordinación, la precisión se mejora a niveles sin precedentes.</p>
				</div>
		</div>

		<!-------------- DISEÑO CELULAR -------------->
		<div class="diseño">
			<img src="../imagenes/mi/diseño.jpg" width="100%" height="auto">
			<div class="textDiseño">
				<h1>La belleza de la confiabilidad Corning® Gorilla® Glass 6</h1>
				<p>La belleza de Mi 9 no está solo en sus colores, sino en su sólida durabilidad. Corning® Gorilla® Glass 6 brinda protección confiable a la pantalla. La cubierta de la cámara está hecha de cristal de zafiro que proporciona una protección duradera.</p>
			</div>	
		</div>

		<!-------------- BOTON CELULAR -------------->		
		<div class="botonAi">
			<div class="textAi">
				<h1>Botón AI independiente. Un botón más hace toda la diferencia</h1>
				<p>Simplemente presione para activar su asistente de Google. El botón también es personalizable. ¿Desea un acceso más fácil a la cámara? ¿Siempre activando el modo de lectura? Ahora puede configurar el botón AI para que lo haga por usted cuando toque dos veces. Una vez que lo pruebes, no querrás dejarlo.</p>
			</div>
			<img src="../imagenes/mi/botonAi.jpg" width="50%">
		</div>

	</div>	
	<footer class="Footer">
		<?php 
			include("../includes/footer.html");
		?>
</body>
</html>
