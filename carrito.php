<?php
	require_once("./back-end/conexion.php");
	session_start();
	@$conectado=$_SESSION['nueva'];
	$sql="SELECT id_usuario FROM Usuarios WHERE email_usuario = '".$_SESSION['nueva']."'";
	$consultaia=mysqli_query($conexion,$sql);
	$id=mysqli_fetch_assoc($consultaia);
	$sql1="SELECT * FROM carrito WHERE id_user = '".$id['id_usuario']."'";
?>	
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">		
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/indexx.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link href="imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">	
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="font/iconos/style.css">
	<link rel="stylesheet" href="css/iniciarSecion.css" />
	<link rel="stylesheet" type="text/css" href="css/carrito.css">
	<link rel="stylesheet" href="css/commerce.css">
	<title>Carrito</title>
	</head>
	<body>
	<?php
	if (isset($_SESSION['nueva'])) {
		echo '<nav class="menuPrincipal">';
			include("includes/menu_sesion.php"); 
		echo '</nav>';	}
	else{
		echo '<nav class="menuPrincipal">';
				include("includes/menu.php"); 
		echo '</nav>';
	}
	?>	
		<div class="contenedorCarrito">
			<div class="infoCarrito">
			<?php	
				echo "<div id='result'></div>";
			?>
			</div>
		    <div class="precioEstimado">
				<div class="cajaEstimada">
					<div class="cerrarDetalles"><i class="fas fa-caret-down" id="cerrarDetallesCarri"></i></div>
					<h1>Resumen del pedido</h1>
					<div class="datos">
						<div class="cajas">
							<p>Subtotal</p>
							<p>Envio</p>
						</div>
						<div class="cajas">
							<p>ARS $0.00</p>
							<p>ARS $0.00</p>
						</div>
					</div>
					<hr class="hr-datos">
					<div class="total">
							<p>Total</p>
							<h3>ARS $0.00</h3>
					</div>
					<div class="detallesCarrito"><i class="fas fa-sort-up" id="detallesCarri"></i></div>
					<div class="botonTotal">
						<a href="">Comprar</a>
					</div>
				</div>
			</div>
		</div>
		<footer class="Footer">
		<?php 
			include("includes/footer.html");
		?>
	</footer>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();

        $("#padre").height(height - 70);
      });
    </script>
    <script> //
		$(document).ready(function(){
			function obtener(){
				$.ajax({
					url: "back-end/carrito_result.php",
					method: "POST",
					success: function(data) {
						$("#result").html(data);
					}
				})
			}
			    obtener();
		$(document).on("click", "#eliminar", function() {
		var id = $(this).data("id");		
		$.ajax({
			url: "back-end/borrar_carrito.php",
			method: "POST",
			data: {id: id},
			success: function(data){
				obtener();
			}
		})
	});
	});		
	</script>
	</body>
	</html>
