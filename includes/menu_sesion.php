<?php
    	require_once("./back-end/conexion.php");
?>

			<div class="menuPrincipal_Logo">
				<a href="index.php"><div class="mihome"></div></a>
				<h1>Xiaomi</h1>
				<div class="menuPrincipal_Logo_sub">
					<div id="preparacionMovil" class='sesionAdapter'>
						<div class="sub">
							<ul>
								<li><a href="ecomerce.php">Productos<i class="fas fa-mobile-alt"></i></a>
									<ul>
										<li><a href="">MI</a></li>
										<li><a href="">Redmi</a></li>
										<li><a href="">Poco</a></li>
										<li><a href="">Todos</a></li>
									</ul>
								</li>
							</ul>
						</div>
							<a href="">Contactenos <i class="fas fa-envelope"></i></a>
							<a href="favoritos.php" id="carri" class='oculto'>Favoritos<i class="fas fa-heart"></i></a>
							<a href="carrito.php"id="carri" class='oculto'>Carrito<i class="fas fa-shopping-cart"></i></a>
					</div>
				</div>
			</div>
			<div class="menuPrincipal_Busqueda">
				<form action="ecomerce.php" method="GET">
					<input type="search" name="busqueda" id="busqueda" placeholder="Buscar productos...">
					<a href="#modal4" id="show-modal4"><span class="icon"><i class="fa fa-search"></i></span></a>
					<div id="oculto"><a href="#modal4" id="show-modal4" ><span class="icon" ><i class="fa fa-search"></i></span></a></div>
				</form>
			</div>
				<div class="inisar" style="display: contents">
						<aside id="modal5" class="modal5">
								<div class="cont-modal5">
									<header>
									<a href="" class="close-modal"><i class="far fa-times-circle"></i></a>
									</header>
									<article>
										<div class="alertEstado" id="estado">
                                              <div class="iconoEstado"><i class="fas fa-exclamation-triangle"></i></div>
                                              <div class="textEstado">
                                                <p>
                                                  Se ha enviado un correo a: <?php echo $_SESSION['nueva']; ?> Por favor, comprueba tu correo
                                                  electronico y haz click en el vinculo del mensaje para completa la
                                                  verificacion de tu cuenta.
                                                </p>
                                                <h3>
                                                  Hasta que su cuenta no sea verificada no podra realizar una compra y si no
                                                  es verificada dentro de los 3 meses (desde su creacion) su cuenta sera
                                                  eliminada.
                                                </h3>
                                              </div>
                                            </div>
									</article>
								</div>
								<a href="" class="btn-close"></a>
							</aside>
					</div>
			<div class="menuPrincipal_User">
				<div class="menuPrincipal_Logo_sub" id='oculto2'>
					<a href="favoritos.php" id="carri"><i class="fas fa-heart"></i></a>
					<a href="carrito.php"id="carri"><i class="fas fa-shopping-cart"></i></a>
				</div>
			
				<div class="menuPrincipal_Logo_sub" id="hola">
				    <a href="includes/cerrar_sesion.php" class="hola" style="display:none"><i class="fas fa-sign-out-alt"></i></a>
					<div class="inisar" id="ocultoMovil"><a href="includes/cerrar_sesion.php" style="text-align: center;
					width: 100%;"><span>Cerrar sesion  </span><i class="fas fa-sign-out-alt"></i></a></div>
					
				</div>
				<div class="menuMovil" onclick="menuMovil()"><i class="fas fa-bars" style="font-size: 20px;color: orangered;" id='prueba'></i></div>
				
				
			</div>