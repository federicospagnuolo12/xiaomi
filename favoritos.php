<?php
	require_once("./back-end/conexion.php");
	session_start();
	if (isset($_GET["id_borrar"])) {
	    include ("borrar_fav.php");
	}
	@$conectado=$_SESSION['nueva'];
?>	
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">		
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/indexx.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<link href="imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">	
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="font/iconos/style.css">
	<link rel="stylesheet" href="css/iniciarSecion.css" />
	<link rel="stylesheet" type="text/css" href="css/favoritos.css">
	<link rel="stylesheet" href="css/commerce.css">
	<title>Favoritos</title>
	</head>
	<body>
	<?php
	if (isset($_SESSION['nueva'])) {
		echo '<nav class="menuPrincipal">';
			include("includes/menu_sesion.php"); 
		echo '</nav>';	}
	else{
		echo '<nav class="menuPrincipal">';
				include("includes/menu.php"); 
		echo '</nav>';
	}
	?>	
		<div class="contenedorFavoritos">
			<div class="infoFavoritos">
			<?php	
				echo "<div id='result'></div>";
			?>
			</div>
		</div>
		<footer class="Footer">
		<?php 
			include("includes/footer.html");
		?>
	</footer>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();

        $("#padre").height(height - 70);
      });
    </script>
    <script> //
		$(document).ready(function(){
			function obtener(){
				$.ajax({
					url: "fav_result.php",
					method: "POST",
					success: function(data) {
						$("#result").html(data);
					}
				})
			}
			    obtener();
		$(document).on("click", "#eliminar", function() {
		var id = $(this).data("id");		
		$.ajax({
			url: "borrar_fav.php",
			method: "POST",
			data: {id: id},
			success: function(data){
				obtener();
			}
		})
	});
	});		
	</script>
	</body>
	</html>
