<?php
	function redimensionarImagen($imagen, $ancho_final, $alto_final){
		list($ancho_orig, $alto_orig, $numero_tipo) = getimagesize($imagen);
		switch ($numero_tipo) {
			case 2:
				$img_original=imagecreatefromjpeg($imagen);
				break;
			
			case 3:
				$img_original=imagecreatefrompng($imagen);
				break;
			default:
				echo "tipo de archivo incorrecto";
				break;
		}
		
				$ratio_ancho=$ancho_final/$ancho_orig;
		
				$ratio_alto=$alto_final/$alto_orig;
		
				$ratio_max=max($ratio_alto, $ratio_ancho);
		
				$nuevo_ancho=$ancho_orig*$ratio_max;
		
				$nuevo_alto=$alto_orig*$ratio_max;
		
				$cortar_ancho=$nuevo_ancho-$ancho_final;
		
				$cortar_alto=$nuevo_alto-$alto_final;

				$desplazamiento=0.5;
		
		$nueva_img=imagecreatetruecolor($ancho_final, $alto_final);
		imagecopyresampled($nueva_img, $img_original, -$cortar_ancho * $desplazamiento, -$cortar_alto * $desplazamiento, 0, 0, $ancho_final + $cortar_ancho, $alto_final + $cortar_alto, $ancho_orig, $alto_orig);
		imagedestroy($img_original);
		$calidad=60;
		$nombre_img=time()."-".$imagen;
		imagejpeg($nueva_img,"../imagenes/Miniaturas/$nombre_img",$calidad);
		return $nombre_img;
	}
?>