<?php
	require_once("conexion.php");
	if (isset($_GET["id_borrar"])) {
	include ("borrar.php");
	}
 ?>
<!DOCTYPE html>
<html>
<head>
    <link href="../imagenes/mi/favicon.jpg" rel="shortcut icon" type="image/x-icon">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="eestilos.css">
	<link rel="stylesheet" type="text/css" href="../css/menu.css">
	<link rel="stylesheet" type="text/css" href="../font/iconos/style.css">
	
	<title></title>
</head>
<body>
	<header>
	<div class="menuPrincipal">
			<div class="menuPrincipal_Logo">
				<a href="Agregar_productos.php"><img src="../imagenes/mihome.png"></a>
				<h1>Xiaomi</h1>
				<div class="menuPrincipal_Logo_sub">
					<div class="sub">
					
						<ul>
							<li>
								<a href="Agregar_productos.php">Productos</a>
								<ul>
									<li><a href="Agregar_productos.php?familia=mi">MI</a></li>
									<li><a href="Agregar_productos.php?familia=redmi">Redmi</a></li>
									<li><a href="Agregar_productos.php?familia=poco">Poco</a></li>
									<li><a href="Agregar_productos.php">Todos</a></li>
								</ul>
								
							</li>
						</ul>
						<a href="usuarios.php">Usuarios</a>
					</div>

				</div>
			</div>
			<div class="menuPrincipal_Busqueda">
				<form action="" method="get">	
					<input type="search" placeholder="Seach...">
					<div class="icono"><a href=""><span class="lnr lnr-magnifier"></span></a></div>
				</form>
			</div>
		</div>
	</header>
	<section>
			<?php
				
				if (isset($_GET["id_editar"])) {
					echo '<div class="insert"  id="fullHeight2">';
					echo "<h1>Editar Producto</h1>";
					echo "<hr class='hr-back'>";
					include ("modificar.php");
					echo "</div>";
					echo '<div class="contactos" id="fullHeight">';
					include("filtros.php");
    				echo '<div id="result"></div>';
                    echo '</div>';
				}
				else{
    				echo '<div class="insert" id="fullHeight2">';
    				echo "<h1>Agregar Producto</h1>";
    				echo "<hr class='hr-back'>";
    				include("insertar.php");
    				echo "</div>";
    				echo '<div class="contactos" id="fullHeight">';
    				echo '<div id="result"></div>';
    				include("filtros.php");
    				echo '</div>';
				}
			?>

	</section>
    
	<div class="btn_insert">
		<span class="lnr lnr-plus-circle"></span>
	</div>
	<div class="btn_list">
		<span class="lnr lnr-circle-minus"></span>
	</div>
		<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();

        $("#fullHeight").height(height - 230);
      });
    </script>
	<script type="text/javascript">
      $(document).ready(function() {
        var height = $(window).height();

        $("#fullHeight2").height(height - 230);
      });
    </script>
	<script>
		$(document).ready(function(){
			function obtener(){
				$.ajax({
					url: "contactos.php",
					method: "POST",
					success: function(data) {
						$("#result").html(data);
					}
				})
			}
			obtener();
		$(document).on("click", "#eliminar", function() {
		var id = $(this).data("id");		
		$.ajax({
			url: "borrar.php",
			method: "POST",
			data: {id: id},
			success: function(data){
				obtener();
			}
		})
	});
	});		
	</script>
	<script type="text/javascript" src="../js/menu.js"></script>
</body>
</html>
