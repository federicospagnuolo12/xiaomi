<?php
	require_once("../back-end/conexion.php");
	$sql="SELECT * FROM productos";
	if (isset($_POST['celus'])){
		$q = $conexion -> real_escape_string($_POST['celus']);
		$sql = "SELECT * FROM productos WHERE nombre LIKE '%".$q."%'";
	}
	$consulta = mysqli_query($conexion, $sql);
	if ($consulta=mysqli_num_rows($consulta) == 0) {
	    echo "Sin resultados.";
	}
	$consulta=mysqli_query($conexion,$sql);
	while ($registro = mysqli_fetch_assoc($consulta)){
		$nombre=$registro['nombre'];
		$precio=$registro['precio'];
		$foto=$registro['IMG'];
		$familia=$registro['familia'];
		$id=$registro['id_productos'];
?>	
		<div class="producto">
			<?php
				echo '<img id="foto" src="imagenes/Miniaturas/'.$foto.'"';	
			?>
			<p class="modelo"><?php echo $nombre;?></p>
			<div class="precioCarrito">
				<strong class="precio">$<?php echo $precio;?></strong>
				<a href="botonCarrito"><i class="fas fa-cart-plus"></i></a>
			</div>
			<div class="boton_compra">
				<?php
					echo '<a href="informacion_celular.php?id_productos='.$id.'"><p>Comprar ahora</p></a>';
				?>
			</div>
		</div>
<?php 
	}
?>