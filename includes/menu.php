<?php
    require_once("./back-end/conexion.php");
 ?>


			<div class="menuPrincipal_Logo">
				<a href="index.php"><div class="mihome"></div></a>
				<h1>Xiaomi</h1>
				<div class="menuPrincipal_Logo_sub">
				<div id="preparacionMovil"><div class="sub">
						<ul>
							<li><a href="ecomerce.php">Productos<i class="fas fa-mobile-alt"></i></a>
								<ul>
									<li><a href="">MI</a></li>
									<li><a href="">Redmi</a></li>
									<li><a href="">Poco</a></li>
									<li><a href="">Todos</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<a href="">Contactenos <i class="fas fa-envelope"></i></a></div>
				</div>
			</div>
					<div class="menuPrincipal_Busqueda">
				<form action="ecomerce.php" method="GET">
					<input type="search" name="busqueda" id="busqueda" placeholder="Buscar productos..." min="3">
					
						<a href="#modal4" id="show-modal4"><span class="icon"><i class="fa fa-search"></i></span></a>
								<aside id="modal4" class="modal4">
									<div class="cont-modal4">
										<article>
											<form action="ecomerce.php" method="GET">
											<div class='busMovil'>
											    <header>
													<div class="closeModalBusquedaMovil"><a href="" class="close-modal"><i class="far fa-times-circle"></i></a></div>
												</header>	
												<input type="search" name="busqueda" id="busqueda" placeholder="Buscar productos..." class='busquedaMovil'>
												<input type="submit" value='Buscar'>
											</div>
											</form>
										</article>
									</div>
								</aside>
							
				</form>
			</div>
			<div class="menuPrincipal_User">
				<div class="menuPrincipal_Logo_sub" class="carr">
					<a href="" id="carri" style="display:none"><i class="fas fa-shopping-cart"></i></a>
				</div>
				<div class="menuPrincipal_Logo_sub">
					<div class="registar">
							<aside id="modal" class="modal">
								<div class="cont-modal">
									<header>
									<a href="" class="close-modal"><i class="far fa-times-circle"></i></a>
									</header>
									<article>
										<?php
											include("registrarse.php");
										?>
									</article>
								</div>
								<a href="" class="btn-close"></a>
							</aside>					
					</div>

					<div id="oculto"><a href="#modal4" id="show-modal4" ><span class="icon" ><i class="fa fa-search"></i></span></a></div>
					<div class="inisar">	
						<a href="#modal2" id="show-modal2"><span>Iniciar Sesion  </span>  <i class="fas fa-sign-in-alt"></i></a>
						<aside id="modal2" class="modal2">
								<div class="cont-modal2">
									<header>
									<a href="" class="close-modal"><i class="far fa-times-circle"></i></a>
									</header>
									<article>
										<?php
											include("iniciar_sesion.php");
										?>
									</article>
								</div>
								<a href="" class="btn-close"></a>
							</aside>
					</div>
					<div class="inisar" id="oculto-modal3">	
					<div class="menuMovil" onclick="menuMovil()"><i class="fas fa-bars" style="font-size: 20px;color: orangered;" id='prueba'></i></div>
						<aside id="modal3" class="modal3">
								<div class="cont-modal3">
									<header>
									<a href="" class="close-modal"><i class="far fa-times-circle"></i></a>
									</header>
									<article>
										<?php
											include("olvidoContraseña.php");
										?>
									</article>
								</div>
								<a href="" class="btn-close"></a>
							</aside>
					</div>
				</div>
				
			</div>
