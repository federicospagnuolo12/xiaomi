<?php
	require_once("./back-end/conexion.php");
?>
    <div class="contenedorIniciarSecion" id="fullHeight">
      <div class="contenedorIniciarSecion_formulario">
        <div class="formulario_titulo">
          <a href="../index.php"><img src="imagenes/mihome.png" width="50px" height="auto"></a>
          <h1>Xiaomi - Iniciar sesión</h1>
        </div>
        <?php
            if (isset($_GET["datosIncorrectos"])) {
                echo '<h2>Datos Incorrectos</h2>';
              
             }
            if (isset($_GET["noInisiado"])) {
                echo '<h2>Inicie sesion antes de continuar</h2>';
             }
              
             
             
        ?>
       <div class="formulario_iniciar">
          <form action="./includes/loguear.php" method="POST">
              <input type="email" id="email" name="email" placeholder="Correo electrónico">
              <input type="password"id="pass"  name="password" placeholder="Contraseña">
              <div class="pass">
                <a href="#modal3"class="volver">He olvidado mi contraseña</a>
              </div>
              <div class="recordarDatos"> 
                <input type="checkbox" name="check" value="check">
                <span>Recordar mis datos</span>
              </div>
              <div class="formulario_boton">
                <input type="submit" name="enviar" value="Iniciar Sesion">    
              </div>
          </form>
       </div>
       <div class="formulario_registro">
          <span>¿No tiene una cuenta? <a href="#modal">Cree una.</a></span>
          
       </div>

      </div>
    </div>



